import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Profile} from '../models/profile';
const BASE_URL = 'https://profiles-list.firebaseio.com/Data.json';
@Injectable({
  providedIn: 'root'
})
export class RequestService {

  profiles: Profile[] = [];

  constructor(
    private http: HttpClient
  ) {}


  private findProfile(id) {
    return this.profiles.find(
      (item) => item.loyalty_member_id === id
    );
  }

  public getProfile(id) {
    return new Promise((resolve, reject) => {
      if (this.profiles.length) {
        resolve(this.findProfile(id));
        return;
      }

      this.getProfiles().subscribe((profiles: Profile[]) => {
        this.profiles = profiles;
        resolve(this.findProfile(id));
      },
      (error) => {
         reject(error);
      });

    });
  }

  public getProfiles() {
    return this.http.get(BASE_URL);
  }
}
