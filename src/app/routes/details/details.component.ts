import {Component, Inject, OnInit} from '@angular/core';
import {Profile} from '../../models/profile';
import { ActivatedRoute } from "@angular/router";
import {RequestService} from '../../services/request.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  id: string;
  loading: boolean = true;
  profile: Profile;

  constructor(
    private route: ActivatedRoute,
    private requestService: RequestService) {
    this.requestService = requestService;
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.requestService.getProfile(this.id).then((profile: Profile) => {
        this.profile = profile;
        this.loading = false;

      });
    });

  }

}
