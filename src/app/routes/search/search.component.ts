import { Component, OnInit, ViewChild } from '@angular/core';
import {RequestService} from '../../services/request.service';
import { Inject } from '@angular/core';
import {Profile} from '../../models/profile';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  providers: [RequestService],
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {
  term: String = '';
  displayedColumns: string[] = [
    'photo',
    'loyalty_member_id',
    'name',
    'phone',
    'address',
    'modified',
    'action'
  ];
  dataSource: MatTableDataSource<Profile>;
  profiles: Profile[] = [];

  loading: Boolean = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private requestService: RequestService) {
    this.requestService = requestService;
  }

  ngOnInit() {
    this.requestService.getProfiles().subscribe((profiles: Profile[]) => {
      this.profiles = profiles;
      this.dataSource = new MatTableDataSource(this.profiles);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.loading = false;

    }, (error) => {
      console.log('Server error:', JSON.stringify(error));
    });
  }

  filter() {
    this.dataSource.filter = this.term.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
