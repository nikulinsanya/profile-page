export class Profile {
  first_name: string;
  last_name: string;
  gender: string;
  prefix: string;
  photo: string;
  phone: string;
  address: string;
  email: string;
  birthdate: string;
  loyalty_member_id: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
