import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {SearchComponent} from './routes/search/search.component';
import {DetailsComponent} from './routes/details/details.component';

const routes: Routes = [
  {
    path: '',
    component: SearchComponent,
  },
  {
    path: 'details/:id',
    component: DetailsComponent,
  },
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: '**', component: SearchComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
